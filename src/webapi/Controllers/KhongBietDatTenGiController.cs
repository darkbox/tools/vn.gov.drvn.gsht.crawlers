using System.Net;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using vn.gov.drvn.gsht.crawlers.Models;

namespace vn.gov.drvn.gsht.crawlers.Controllers;

[ApiController]
[Route("[controller]")]
public class KhongBietDatTenGiController : ControllerBase
{
    [HttpPost("KiemTraViTriHienTaiVaLanCapNhatCuoiCuaXe")]
    public async Task<ViTriHienTaiVaLanCapNhatCuoiCuaXeModel> KiemTraViTriHienTaiVaLanCapNhatCuoiCuaXe (ViTriHienTaiVaLanCapNhatCuoiCuaXeParameter parameter) {

        // "https://gsht.drvn.gov.vn/lshtagl?action=list&h=7813"

        var JSESSIONID = md5($"vn.gov.drvn.gsht.crawlers.session({parameter.TenTaiKhoan}:{parameter.MatKhau})");

        var cli = new HttpClient();

        cli.DefaultRequestHeaders.Clear();
        cli.DefaultRequestHeaders.Add("Cookie", $"JSESSIONID={JSESSIONID};");
        cli.DefaultRequestHeaders.Add("User-Agent", "vn.gov.drvn.gsht");

        var denNgay = ((long)(DateTimeOffset.UtcNow - new DateTimeOffset(1970, 1, 1, 0, 0, 0, new TimeSpan(0,0,0))).TotalMilliseconds);
        var tuNgay = ((long)((DateTimeOffset.UtcNow-new TimeSpan(24,0,0)) - new DateTimeOffset(1970, 1, 1, 0, 0, 0, new TimeSpan(0, 0, 0))).TotalMilliseconds);
        try
        {

            var resp = await cli.PostAsync(
                "https://gsht.drvn.gov.vn/lshtagl?action=list&h=7813",
                new StringContent(@$"{{""mapParam"":{{""bienSo"":""{parameter.BienSo}"",""tuNgay"":""{tuNgay}"",""denNgay"":""{denNgay}""}}}}", null, "application/json")
            );

            if (resp is null || resp.Content.Headers.ContentType is null)
            {
                return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel { Error = "Máy chủ GSHT phản hồi không hợp lệ!" };
            }

            if (resp.Content.Headers.ContentType.MediaType == "text/html")
            {
                // session expire or un-initial, do login-process

                var loginResp = await cli.PostAsync(
                    "https://gsht.drvn.gov.vn/loginagl?action=login&h=7790",
                    new StringContent(@$"{{""mapParam"":{{""ma_nsd"":""{parameter.TenTaiKhoan}"",""mat_khau"":""{parameter.MatKhau}""}}}}", null, "application/json")
                );

                if (loginResp is null || loginResp.Content.Headers.ContentType is null)
                {
                    return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel { Error = "Máy chủ GSHT phản hồi không hợp lệ!" };
                }
                var loginMsg = Encoding.UTF8.GetString(await loginResp.Content.ReadAsByteArrayAsync());

                if (loginMsg is null || loginMsg.Contains("{\"errCode\":\"1\""))
                {
                    return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel { Error = "Thông tin xác thực không hợp lệ!" };
                }

                // login-done, re-fetch data
                resp = await cli.PostAsync(
                    "https://gsht.drvn.gov.vn/lshtagl?action=list&h=7813",
                    new StringContent(@$"{{""mapParam"":{{""bienSo"":""{parameter.BienSo}"",""tuNgay"":""{tuNgay}"",""denNgay"":""{denNgay}""}}}}", null, "application/json")
                );
            }

            if (resp.Content.Headers.ContentType is null || resp.Content.Headers.ContentType.MediaType != "application/json")
            {
                return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel { Error = "Máy chủ GSHT phản hồi không hợp lệ!" };
            }

            var data = Encoding.UTF8.GetString(await resp.Content.ReadAsByteArrayAsync());

            if (data is not null && data.Contains(@"""msg"":{""errCode"":""0""") && data.Contains(@"""listHistory"":""["))
            {
                var json = JsonConvert.DeserializeObject<JObject>(data);

                var listHistoryTxt = json?["msg"]?["object"]?["listHistory"]?.Value<string>();

                var listHistory = JsonConvert.DeserializeObject<JArray>(listHistoryTxt??"[]");
                if (listHistory is null || listHistory.Count == 0)
                {
                    return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel()
                    {
                        Error = "Không tìm thấy dữ liệu!",
                    };
                }

                var lastHistory = listHistory.LastOrDefault();

                if (lastHistory is null)
                {
                    return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel()
                    {
                        Error = "Không tìm thấy dữ liệu!",
                    };
                }

                return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel()
                {
                    LastTime = $"{lastHistory["stime"]}",
                    LastLatitude = $"{lastHistory["latitude"]}",
                    LastLongitude = $"{lastHistory["longitude"]}",
                };
            }
            return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel()
            {
                Error = "Không tìm thấy dữ liệu!",
            };
        }
        catch
        {
            return new ViTriHienTaiVaLanCapNhatCuoiCuaXeModel { Error = "Không kết nối được dịch vụ GSHT!" };
        }
    }

    static string md5(string input)
    {
        MD5 md5Hasher = MD5.Create();
        byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
        return BitConverter.ToString(data).Replace("-", "");
    }
}