namespace vn.gov.drvn.gsht.crawlers.Models;

public class ViTriHienTaiVaLanCapNhatCuoiCuaXeParameter {
    public string TenTaiKhoan {get;set;}
    public string MatKhau {get;set;}
    public string BienSo {get;set;}
}