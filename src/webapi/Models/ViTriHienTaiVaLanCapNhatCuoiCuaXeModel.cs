namespace vn.gov.drvn.gsht.crawlers.Models;

public class ViTriHienTaiVaLanCapNhatCuoiCuaXeModel
{
    public string? Error { get; set; } = null;

    public string LastLatitude { get; set; } = "";
    public string LastLongitude { get; set; } = "";
    public string LastTime { get; set; } = "";
}